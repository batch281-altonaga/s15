// Comments

// Comments are parts of the code that gets ignored by the language
// Comments are meant to describe the written code

/* Two types of comments
    1. Single Line
    2. Multi Line
*/

console.log("Hello, World!");

// Variables
/* 
  - it is used to contain data
*/

// Declaring Variables - tells our devices that a variable name is created and is ready to store data

/* 
    Syntax:
        let/const variableName = value;
        // camel-casing - common method of naming variables

        var vs let
        var - used to declare global variables
        let - used to declare local variables

        let - keyword usually used in declaring a variable
        const - keyword usually used in declaring a CONSTANT variable
*/

let myVar;
console.log(myVar);

let productName = "desktop computer";
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;

// Reassigning variable values
// Syntax: variableName =

productName = "Laptop";
console.log(productName);

//interest = 4.489;

// Declares a variable first
let supplier;
// Initialization of value
supplier = "John Smith Tradings";

// Multiple variable declaration

let productCode = "DCO17",
  productBrand = "Dell";

console.log(productCode, productBrand);

// Data types

// String - handles a series of characters that create a word, phrase, or a sentence.
let country = "Philippines";
let province = "Metro Manila";

let fullAddress = province + ", " + country;

console.log(fullAddress);
let greeting = "I live in the " + country;
console.log(greeting);

// Escape Character (\) in strings is a combination with other characters that can produce a different effect (\n)
let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);

// Numbers
// Integers / Whole Numbers
let headcount = 32;
console.log(headcount);

// Floats / Decimals
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining text/numbers and strings
console.log("John's grade last quarter is " + grade);

// Boolean - used to store values relating to the state of certain things
let isMarried = true;
let inGoodConduct = false;
console.log("isMarried: " + isMarried);
console.log("inGoodConduct: " + inGoodConduct);

// Array - special kind of data type that is used to store multiple values
// Syntax: let/const arrayName = [elementA, elementB, elementC, ...];

let grades = [98.7, 92.1, 90.2, 94.6];

console.log(grades);

// Objects - another special kind of data type that's used to mimic real-world objects/items
/* 
    Syntax:
    let/const objectName = {
      propertyA : value,
      propertyB : value,
    }
*/
let person = {
  fullName: "Juan Dela Cruz",
  age: 35,
  isMarried: false,
  contact: ["0917 123 4567", "8123 4567"],
  address: {
    houseNumber: "345",
    city: "Manila",
  },
};

console.log(person);

let myGrades = {
  firstGrading: 98.7,
  secondGrading: 92.1,
  thirdGrading: 90.2,
  fourthGrading: 94.6,
};

console.log(myGrades);

// typeof operator - used to determining the type of data or the value of the variable.
console.log(typeof myGrades);
console.log(typeof grades); // this is an array, but the console will log "object" - because array is a special type of object

// Null - used to intentionally express the absence of a value in a variable declaration/initialization
let spouse = null;
let myNumber = 0;
let myString = "";

// Undefined
let fullName;
console.log(fullName);
